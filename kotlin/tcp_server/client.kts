package client

import java.io.OutputStream
import java.net.Socket
import java.nio.charset.Charset

import kotlin.concurrent

fun main(args: Array<String>) {
    try {
        val client: Socket = Socket("0.0.0.0", 7000)
        val writer: OutputStream = client.getOutputStream()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    val data = "Some data to send"

    while (true) {
        writer.write(data.toByteArray(Charset.defaultCharset()))
        println("Data sent: ${data}")
        
        Thread.sleep(1000)
    }
}

main()
