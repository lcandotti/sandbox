package server

import java.io.OutputStream
import java.net.Socket
import java.net.ServerSocket
import java.nio.charset.Charset
import java.util.Scanner

import kotlin.concurrent.thread

/**
 * @param client the server's client socket to respond to any request
 */
fun handler(client: Socket) {

    /** 
     * @param c the server's client socket to response to any request
     */
    fun shutdown(c: Socket) {
        c.close()
        println("Client ${client.inetAddress.hostAddress} closed the connection")
    }

    /**
     * @param w the object that will write to the socket
     * @param msg the message to send
     */
    fun write(w: OutputStream, msg: String) {
        w.write("${msg}\n".toByteArray(Charset.defaultCharset()))
    }

    val reader: Scanner = Scanner(client.getInputStream())
    val writer: OutputStream = client.getOutputStream()

    var recv: String? = null

    write(writer, "Welcome to the server!\nTo exit, send: 'EXIT'")

    while (true) {
        try {
            recv = reader.nextLine()
            if (recv == "EXIT") {
                shutdown(client)
                continue
            }
        } catch (ex: Exception) {
            shutdown(client)
        } finally {
            write(writer, if (recv != null) recv else "")
        }
    }
}

fun main() {
    val server: ServerSocket = ServerSocket(7000)

    println("Starting the server at 0.0.0.0:7000")
    println("Quit the server with CONTROL-C")

    while (true) {
        val client = server.accept()

        println("Client connected: ${client.inetAddress.hostAddress}")

        thread { handler(client) }
    }
}

main()
