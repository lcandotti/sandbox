defmodule Greet do
  def hello do
    IO.puts "Hello, from elixir !"
  end

  def hello(name) do
    IO.puts "Hello, " <> name <> " !"
  end

  def hello(name_lhs, name_rhs) do
    IO.puts "Hello, #{name_lhs} and #{name_rhs} !"
  end
end

IO.puts Greet.hello
IO.puts Greet.hello "John"
IO.puts Greet.hello "John", "Doe"
