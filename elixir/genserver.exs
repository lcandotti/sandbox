defmodule KV.Counter do
  @moduledoc """
  This module is a simple example on how one uses a GenServer to handle state
  management within elixir. In this example we have built a simple counter that
  we can increment.

  ## Example

    iex> KV.Counter.start_link []
    :ok
    iex> KV.Counter.increment
    :ok
    iex> KV.Counter.increment
    :ok
    iex> IO.puts "Counter value: #{KV.Counter.get_value}"
  """
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, 0, name: :counter)
  end

  def init(value) do
    {:ok, value}
  end

  def increment do
    GenServer.cast(:counter, :increment)
  end

  def get_value do
    GenServer.call(:counter, :get_value)
  end

  def handle_call(:get_value, _from, state) do
    {:reply, state, state}
  end

  def handle_cast(:increment, state) do
    nstate = state + 1
    {:noreply, nstate}
  end
end

{:ok, _pid} = KV.Counter.start_link []
KV.Counter.increment
KV.Counter.increment

IO.puts "Counter value: #{KV.Counter.get_value}"
