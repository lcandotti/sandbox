defmodule Greet do
  defp phrase("en") do
    "Hello, "
  end

  defp phrase("fr") do
    "Bonjour, "
  end

  def hello(name, language_code \\ "en") do
    phrase(language_code) <> name <> " !"
  end
end

IO.puts Greet.hello "John"
IO.puts Greet.hello "Sean", "fr"
