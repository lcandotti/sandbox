handle = fn
  {:ok, result} -> IO.puts("> Handle result: #{result}")
  {:ok, _} -> IO.puts("> Handle is unreachable")
  {:error} -> IO.puts("> An error as occured")
end

# Success
handle.({:ok, true})

# Error
handle.({:error})
