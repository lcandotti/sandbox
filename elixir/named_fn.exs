defmodule Length do
  def of([]) do
    0
  end

  def of([_ | tail]) do
    1 + of(tail)
  end
end

IO.puts "Len: #{Length.of([])}"
IO.puts "Len: #{Length.of([1, 2, 3])}"
