defmodule KV do
  @moduledoc """
  Documentation for `KV`.
  """

  @doc """
  Return the library current version
  """
  def version do
    "0.1.0"
  end
end
