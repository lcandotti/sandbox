defmodule KV.Bucket do
  use Agent

  @moduledoc"""
  A bucket is a data structure that act the same way as a global hashmap
  """

  @doc"""
  Start a new bucket storage
  """
  def start_link(_opts) do
    Agent.start_link(fn -> %{} end)
  end

  @doc"""
  Get a value from the `bucket` by `key`
  """
  def get(bucket, key) do
    Agent.get(bucket, &Map.get(&1, key))
  end

  @doc"""
  Put the `value` for the provided `key` in the bucket
  """
  def put(bucket, key, value) do
    # Client code here
    Agent.update(bucket, fn state ->
      # Server code here
      Map.put(state, key, value)
    end)
  end

  @doc"""
  Delete `key` from `bucket`

  Returns the current value of `key` if `key` exists
  """
  def delete(bucket, key) do
    # Client code here
    Agent.get_and_update(bucket, fn state ->
      # Server code here
      Map.pop(state, key)
    end)
  end
end
