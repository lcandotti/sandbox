defmodule KVTest do
  use ExUnit.Case, async: true

  test "retrieve program current version" do
    assert KV.version() == "0.1.0"
  end
end
