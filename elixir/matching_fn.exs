defmodule Greet do
  @doc """
  Function pattern matching 
  """
  def hello(%{name: name}) do
    IO.puts "Hello, " <> name <> " !"
  end

  @doc """
  Function pattern matching w/ retention
  """
  def hello(%{name: name} = person) do
    IO.puts "Hello, " <> name <> " !"
    IO.inspect person
  end
end

john = %{name: "John", age: 47}

Greet.hello john
