defmodule Greet do
  @doc """
  This function is declared as private and so can't be called from the Greet module
  """
  defp phrase do
    "Hello, "
  end
  
  def hello(name) do
    phrase <> name <> " !"
  end
end

Greet.hello "John"
