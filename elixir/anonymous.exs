# Basic anonymous function
sum = fn (a, b) -> a + b end

IO.puts "Total base anonymous: #{sum.(1, 2)}"

# Shorthand anonymous function
sum = &(&1 + &2)

IO.puts "Total shorthand anonymous: #{sum.(1, 2)}"
