defmodule Greet do
  def hello(names) when is_list names do
    names = Enum.join names, ", "

    hello names
  end

  def hello(name) when is_binary name do
    IO.puts "Hello, " <> name <> " !"
  end
end

Greet.hello ["John", "Sean"]
