#ifndef __CROSS_H__
#define __CROSS_H__

#include <stdio.h>

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

#ifdef _WIN32
    typedef unsigned __int8 BOOL;

    typedef __int64 INT64;
    typedef unsigned __int64 U_INT64;
    
    typedef __int32 INT32;
    typedef unsigned __int32 U_INT32;
#else
    #include <stdint.h>

    typedef uint8_t BOOL;

    typedef int64_t INT64;
    typedef uint64_t U_INT64;
    
    typedef int32_t INT32;
    typedef uint32_t U_INT32;
#endif

#endif
