#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <xcb/xcb.h>
#include <xcb/xproto.h>

int main(int argc, char *argv[]) {
  // Open to connection to the X server
  xcb_connection_t *connection = xcb_connect(NULL, NULL);

  // Get the first screen
  const xcb_setup_t *setup = xcb_get_setup(connection);

  xcb_screen_iterator_t screen_iterator = xcb_setup_roots_iterator(setup);
  xcb_screen_t *screen = screen_iterator.data;

  // Ask the X server to generate a memory access to our window
  xcb_window_t window = xcb_generate_id(connection);

  // Define Window properties
  uint32_t masks = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
  uint32_t values[2] = {
      screen->white_pixel,
      XCB_EVENT_MASK_KEY_PRESS,
  };

  // Create a window
  xcb_create_window(
      connection,
      XCB_COPY_FROM_PARENT,
      window,
      screen->root,
      0,
      0,
      150,
      150,
      10,
      XCB_WINDOW_CLASS_INPUT_OUTPUT,
      screen->root_visual,
      masks,
      &values
  );

  // Map the window on the screen
  xcb_map_window(connection, window);

  // Flush all pending requests from the connection
  xcb_flush(connection);

  // Run the main event loop that will hold the window
  bool disconnect = false;
  xcb_generic_event_t *event;

  while (disconnect == false && (event = xcb_wait_for_event(connection))) {
    switch (event->response_type & ~0x80) {
    case XCB_KEY_PRESS: {
      xcb_key_press_event_t *kp = (xcb_key_press_event_t *)event;

      // Keycode 24 -> 'q' (on qwertz)
      if (kp->detail == 24) {
        disconnect = true;
      }

      break;
    }
    default: {
      // Unknown event type. Ignore it
      break;
    }
    }

    // Free the generic event
    free(event);
  }

  // Disconnect from X server
  xcb_disconnect(connection);

  return 0;
}
