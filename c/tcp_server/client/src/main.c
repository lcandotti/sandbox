#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "tz.h"

#define BUFFER_SIZE 1024

int main(int argc, char *argv[]) {
  int port = atoi(argv[1]);

  int server_fd;
  struct sockaddr_in server_addr;

  server_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (server_fd < 0) {
    fprintf(stderr, "Unable to create socket\n");
    return -1;
  }

  bzero((char *)&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = inet_addr("0.0.0.0");

  int opt_val = 1;
  setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

  if (connect(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
    fprintf(stderr, "Unable to connect to the server\n");
    return -2;
  }

  char message[12] = "hello server";

  while(1) {
    if (write(server_fd, message, strlen(message)) < 0) {
      fprintf(stderr, "Unable to write to server file descriptor\n");
      break;
    }

    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));

    if (read(server_fd, buffer, sizeof(buffer)) < 0) {
      fprintf(stderr, "Unable to read from server socket\n");
      break;
    }

    printf("Received from server: %s\n", buffer);
  }

  close(server_fd);

  return 0;
}
