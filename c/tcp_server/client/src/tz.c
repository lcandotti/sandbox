#include <stdio.h>
#include <time.h>

void tz_now(char *buffer, size_t buffer_size) {
  int hours, minutes, seconds, day, month, year;

  time_t now;
  time(&now);

  struct tm *local = localtime(&now);

  hours = local->tm_hour;
  minutes = local->tm_min;
  seconds = local->tm_sec;
  day = local->tm_mday;
  month = local->tm_mon + 1;
  year = local->tm_year + 1900;

  sprintf(buffer, "%d %d, %d - %d:%d:%d", month, day, year, hours, minutes, seconds);
}
