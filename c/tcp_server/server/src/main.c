#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "tz.h"

#define BUFFER_SIZE 1024

int main(int argc, char *argv[]) {
  int port = atoi(argv[1]);

  int server_fd, client_fd;
  struct sockaddr_in server_addr, client_addr;

  server_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (server_fd < 0) {
    fprintf(stderr, "Unable to create socket\n");
    return -1;
  }

  bzero((char *)&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  int opt_val = 1;
  setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

  if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
    fprintf(stderr, "Unable to bind socket\n");
    return -2;
  }

  if (listen(server_fd, 16) < 0) {
    fprintf(stderr, "Unable to listen socket\n");
    return -3;
  }

  char tz_buffer[255];
  tz_now(tz_buffer, sizeof(tz_buffer));

  printf("%s\n", tz_buffer);
  printf("Starting server on localhost:%d\n", port);
  printf("Quit the server with CONTROL-C\n");

  socklen_t client_addr_size = sizeof(client_addr);

  while(true) {
    client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &client_addr_size);
    if (client_fd < 0) {
      fprintf(stderr, "Unable to accept new connection on socket\n");
      return -4;
    }

    printf("New connection from %s:%d (file descriptor: %d)\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), client_fd);

    char buffer[BUFFER_SIZE];
    memset(buffer, 0, BUFFER_SIZE);

    int read_size = read(client_fd, buffer, BUFFER_SIZE);
    if (read_size < 0) {
      fprintf(stderr, "Unable to read received response\n");
      return -5;
    }

    printf("Received from client: %s\n", buffer);

    if (write(client_fd, buffer, read_size) < 0) {
      fprintf(stderr, "Unable to write buffer to client file descriptor\n");
      return -6;
    }

    close(client_fd);
  }

  close(server_fd);

  return 0;
}
