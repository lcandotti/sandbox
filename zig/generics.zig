const std = @import("std");

pub fn Queue(comptime Child: type) type {
    return struct {
        const This = @This();
        const Node = struct {
            data: Child,
            next: ?*Node,
        };

        gpa: std.mem.Allocator,
        start: ?*Node,
        end: ?*Node,

        pub fn init(gpa: std.mem.Allocator) This {
            return This{
                .gpa = gpa,
                .start = null,
                .end = null,
            };
        }

        pub fn enqueue(this: *This, value: Child) !void {
            const node = try this.gpa.create(Node);

            node.* = .{ .data = value, .next = null };

            if (this.end) |end| end.next = node //
            else this.start = node;

            this.end = node;
        }

        pub fn dequeue(this: *This) ?Child {
            const start = this.start orelse return null;
            defer this.gpa.destroy(start);

            if (start.next) |next|
                this.start = next
            else {
                this.start = null;
                this.end = null;
            }

            return start.data;
        }
    };
}

test "generics" {
    var queue = Queue(i32).init(std.testing.allocator);

    try queue.enqueue(25);
    try queue.enqueue(50);
    try queue.enqueue(75);
    try queue.enqueue(100);

    try std.testing.expectEqual(queue.dequeue(), 25);
    try std.testing.expectEqual(queue.dequeue(), 50);
    try std.testing.expectEqual(queue.dequeue(), 75);
    try std.testing.expectEqual(queue.dequeue(), 100);
    try std.testing.expectEqual(queue.dequeue(), null);

    try queue.enqueue(5);

    try std.testing.expectEqual(queue.dequeue(), 5);
    try std.testing.expectEqual(queue.dequeue(), null);
}
