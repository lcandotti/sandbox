defmodule KV do
  @moduledoc """
  Simple module that host a hello world function
  """
  def main do
    IO.puts "Hello world!"
  end
end

KV.main
