#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wtypes.h>

int Add(int __l, int __r) {
    return __l + __r;
}

int ReturnZero(int left, int right) {
    return 0;
}

void InstallHook(void* func2hook, void* payload) {
    DWORD oldProtect;
    VirtualProtect(Add, 1024, PAGE_EXECUTE_READWRITE, &oldProtect);

    // 32 bits relative jump opcode
    uint8_t jumpInstruction[5] = { 0xE9, 0x0, 0x0, 0x0, 0x0 };

    const uint8_t relativeAddress = (uint8_t)payload - ((uint8_t)func2hook - sizeof(jumpInstruction));
    memcpy(jumpInstruction + 1, &relativeAddress, 4);

    // Install the hook
    memcpy(func2hook, jumpInstruction, sizeof(jumpInstruction));
}

int main() {
    // Install a hook in Add, going to return 0
    InstallHook(Add, ReturnZero);

    int num = Add(1, 1);
    printf("%d\n", num); // Will always print 0

    return 0;
}
