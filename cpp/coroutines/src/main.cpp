#include <iostream>
#include <vector>

generator<int> generateNumbers(int begin, int inc = 1) {
    for (int i = begin ;; i += inc) {
        co_yield i;
    }
}

int main(int argc, char** argv) {
    std::cout << std::endl;

    const auto numbers = generateNumbers(-10);

    for (int i = 1; i <= 20; i++) {
        std::cout << numbers << " ";
    }

    for (auto n: generateNumbers(0, 5)) {
        std::cout << n << " ";
    }

    std::cout << "\n\n";
}
