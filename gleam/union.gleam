type IntOrFloat {
  AnInt(Int)
  AFloat(Float)
}

pub fn int_or_float(x: IntOrFloat) {
  case x {
    AnInt(1) -> "Integer: 1"
    AFloat(1.0) -> "Float: 1.0"
  }
}
