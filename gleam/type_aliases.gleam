pub type Headers =
  List(#(String, String))

pub fn main() {
  let headers: Headers = [#("Content-Type", "application/json")]
}
