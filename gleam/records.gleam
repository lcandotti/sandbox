type Person {
  Person(name: String, age: Int)
}

pub fn main() {
  let person = Person(name: "John Doe", age: 27)
  let name = person.name
}
