package coroutines

import (
	"fmt"
	"net/http"
	"testing"
)

func assertEq[T comparable](t *testing.T, expected T, actual T) {
	// Enable testing
	t.Helper()
	// Statement
	if expected == actual {
		return
	}
	// Error message in case stmt didn't catch values
	t.Errorf("Expected %v, but got %v", expected, actual)
}

// TestCoroutines ...
func TestCoroutines(t *testing.T) {
	// Goroutine channel
	messages := make(chan string)
	// Anonymous goroutine
	go func() {
		messages <- "ping"
	}()
	// Tests
	assertEq(t, <-messages, "ping")
}

// TestHTTPCoroutines ...
func TestHTTPCoroutines(t *testing.T) {
	// Base urls
	urls := []string{"https://example.com", "https://example.org", "https://example.net"}
	// Goroutine channel
	responses := make(chan *http.Response)
	// Process urls concurrently
	for _, url := range urls {
		// Anonymous goroutine
		go func() {
			resp, err := http.Get(url)
			if err != nil {
				fmt.Printf("Client error: %s\n", err)
			} else {
				responses <- resp
			}
		}()
	}
	// Tests fixtures
	res := <-responses
	// Tests
	assertEq(t, res.StatusCode, 200)
}
