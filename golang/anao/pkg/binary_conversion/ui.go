package binary_conversion

import (
	"strconv"
	"strings"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
)

func binaryToText(data string) string {
	data = strings.ReplaceAll(data, " ", "")

	res := ""
	for i := 0; i < len(data); i += 8 {
		val, _ := strconv.ParseInt(data[i:i+8], 2, 8)
		res += string(rune(val))
	}

	return res
}

type Model struct {
	Help      help.Model
	Keys      KeyMap
	Result    string
	TextInput textinput.Model
}

func New() Model {
	model := Model{
		Help:      help.New(),
		Keys:      Keys,
		TextInput: textinput.New(),
	}

	model.TextInput.Placeholder = "01101100 10100000 ..."
	model.TextInput.Focus()

	return model
}

func (m Model) Init() tea.Cmd {
	return textinput.Blink
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.Keys.Enter):
			m.Result = binaryToText(m.TextInput.Value())

		case key.Matches(msg, m.Keys.Quit):
			return m, tea.Quit
		}
	}

	m.TextInput, cmd = m.TextInput.Update(msg)

	return m, cmd
}

func (m Model) View() string {
	s := strings.Builder{}

	s.WriteString("\nInput the binary string you want to convert to text:\n\n")
	s.WriteString(m.TextInput.View())
	s.WriteString("\n\n")

	if m.Result != "" {
		s.WriteString("Result\n")
		s.WriteString("----------------\n")
		s.WriteString(m.Result)
		s.WriteString("\n\n")
	}

	s.WriteString(m.Help.View(m.Keys))

	return s.String()
}
