package pokewordle

import "github.com/charmbracelet/lipgloss"

var (
	BannerStyle  = lipgloss.NewStyle().Bold(true).Border(lipgloss.RoundedBorder()).MarginLeft(2).MarginTop(2).PaddingLeft(1).PaddingRight(1).Foreground(lipgloss.Color("#CAD3F5"))
	TitleStyle   = lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("#C6A0F6"))
	SpacingStyle = lipgloss.NewStyle().MarginLeft(4)
	ResultStyle  = lipgloss.NewStyle().Foreground(lipgloss.Color("#494D64"))
	CorrectStyle = lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("#A6DA95"))
	MaybeStyle   = lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("#F5A97F"))
	WrongStyle   = lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("#A5ADCB"))
	HelpStyle    = lipgloss.NewStyle().MarginLeft(2)
)
