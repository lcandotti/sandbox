package pokewordle

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strings"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/textinput"

	tea "github.com/charmbracelet/bubbletea"
)

const API_ENDPOINT = "https://pokeapi.co/api/v2/pokemon?limit=151&offset=151"

type Pokemon struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}

type Response struct {
	Count    int       `json:"count"`
	Next     *string   `json:"next"`
	Previous *string   `json:"previous"`
	Results  []Pokemon `json:"results"`
}

func getRandom(list []Pokemon) Pokemon {
	return list[rand.Intn(len(list))]
}

func getWordle() string {
	response, err := http.Get(API_ENDPOINT)
	if err != nil {
		log.Fatalf("Couldn't reach host '%s': %s", API_ENDPOINT, err)
	}
	defer response.Body.Close()

	var data Response

	err = json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		log.Fatalf("Couldn't parse response body: %s", err)
	}

	return getRandom(data.Results).Name
}

var answer = getWordle()

func checkResult(result string) (string, bool) {
	mr := strings.Split(result, "")
	ma := strings.Split(answer, "")

	c := 0

	for i := 0; i < len(mr); i++ {
		if mr[i] == ma[i] {
			c += 1
			mr[i] = CorrectStyle.Render(mr[i])
		} else if strings.Contains(answer, mr[i]) {
			mr[i] = MaybeStyle.Render(mr[i])
		} else {
			mr[i] = ResultStyle.Render(mr[i])
		}
	}

	result = strings.Join(mr, "")

	return result, c == len(ma)
}

type Model struct {
	Found   bool
	Help    help.Model
	Input   textinput.Model
	Keys    KeyMap
	Limit   int
	Results []string
	Tries   int
}

func New() Model {
	input := textinput.New()
	input.Placeholder = strings.Repeat("*", len(answer))
	input.Focus()
	input.CharLimit = len(answer)
	input.Prompt = ""

	return Model{
		Help:    help.New(),
		Input:   input,
		Keys:    Keys,
		Limit:   10,
		Results: make([]string, 0),
		Tries:   0,
	}
}

func (m Model) Init() tea.Cmd {
	return textinput.Blink
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.Keys.Quit):
			return m, tea.Quit
		case key.Matches(msg, m.Keys.Enter):
			if m.Found {
				return m, tea.Quit
			}

			result := m.Input.Value()

			if result != "" {
				result, ok := checkResult(result)
				m.Results = append(m.Results, result)

				if ok {
					m.Found = true
				} else {
					m.Tries += 1
				}
			}

			m.Input.Reset()
		}
	}

	m.Input, cmd = m.Input.Update(msg)

	return m, cmd
}

func (m Model) View() string {
	s := strings.Builder{}

	if m.Tries >= m.Limit {
		s.WriteString(SpacingStyle.MarginTop(1).Render("You reached the limit of tries !"))
		s.WriteString("\n\n")
		s.WriteString(SpacingStyle.Render("The word was: "))
		s.WriteString(ResultStyle.Render(answer))

		return s.String()
	}

	if m.Found {
		s.WriteString(SpacingStyle.MarginTop(1).Render("Congratulation you found the word of the day !"))
		s.WriteString("\n\n")
		s.WriteString(SpacingStyle.Render(m.Help.View(m.Keys)))

		return s.String()
	}

	s.WriteString(BannerStyle.Render("What's today pokémon wordle ?"))
	s.WriteString("\n\n")
	s.WriteString(SpacingStyle.Render(TitleStyle.Render("Prompt")))
	s.WriteString("\n")
	s.WriteString(SpacingStyle.Render(m.Input.View()))
	s.WriteString("\n\n")
	s.WriteString(SpacingStyle.Render(TitleStyle.Render("Results")))
	s.WriteString("\n")

	if len(m.Results) == 0 {
		s.WriteString(SpacingStyle.Render(ResultStyle.Render(strings.Repeat(".", len(m.Input.Placeholder)))))
	} else {
		for _, result := range m.Results {
			s.WriteString(SpacingStyle.Render(ResultStyle.Render(result)))
			s.WriteString("\n")
		}
	}

	s.WriteString("\n\n")
	s.WriteString(HelpStyle.Render(m.Help.View(m.Keys)))

	return s.String()
}
