package ui

import (
	"fmt"
	"strings"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	tea "github.com/charmbracelet/bubbletea"

	"gitlab.com/lcandotti/anao/pkg/binary_conversion"
	"gitlab.com/lcandotti/anao/pkg/pokewordle"
)

type Model struct {
	Choices []View
	Choosen *int
	Cursor  int
	Help    help.Model
	Keys    KeyMap
}

func New() Model {
	return Model{
		Choices: []View{
			{Name: "Convert binary to text", Model: binary_conversion.New()},
			{Name: "Play a game of Poké-Wordle", Model: pokewordle.New()},
		},
		Choosen: nil,
		Help:    help.New(),
		Keys:    Keys,
	}
}

func (m Model) Init() tea.Cmd {
	return nil
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.Keys.Quit):
			return m, tea.Quit

		case key.Matches(msg, m.Keys.Up):
			if m.Cursor > 0 {
				m.Cursor--
			}

		case key.Matches(msg, m.Keys.Down):
			if m.Cursor < len(m.Choices)-1 {
				m.Cursor++
			}

		case key.Matches(msg, m.Keys.Help):
			m.Help.ShowAll = !m.Help.ShowAll

		case key.Matches(msg, m.Keys.Enter):
			m.Choosen = &m.Cursor
			v := m.Choices[*m.Choosen]
			return v.Model.Update(msg)
		}
	}

	return m, nil
}

func (m Model) View() string {
	s := strings.Builder{}
	s.WriteString(BannerStyle.Render("What are we doing today ?"))
	s.WriteString("\n\n")

	for i, choice := range m.Choices {
		if m.Choosen != nil {
			return choice.Model.View()
		}

		var opt string

		if m.Cursor == i {
			opt = SelectedChoiceStyle.Render(fmt.Sprintf("%s %s", "●", choice.Name))
		} else {
			opt = ChoiceStyle.Render(fmt.Sprintf("%s %s", "○", choice.Name))
		}

		s.WriteString(opt)
		s.WriteString("\n")
	}

	s.WriteString("\n")
	s.WriteString(HelpStyle.Render(m.Help.View(m.Keys)))

	return s.String()
}
