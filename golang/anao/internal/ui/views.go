package ui

import tea "github.com/charmbracelet/bubbletea"

type View struct {
	Name  string
	Model tea.Model
}
