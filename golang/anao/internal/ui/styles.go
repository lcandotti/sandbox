package ui

import "github.com/charmbracelet/lipgloss"

var (
	BannerStyle         = lipgloss.NewStyle().Bold(true).Border(lipgloss.RoundedBorder()).MarginLeft(2).MarginTop(2).PaddingLeft(1).PaddingRight(1).Foreground(lipgloss.Color("#CAD3F5"))
	ChoiceStyle         = lipgloss.NewStyle().MarginLeft(4).Foreground(lipgloss.Color("#CAD3F5"))
	SelectedChoiceStyle = lipgloss.NewStyle().MarginLeft(4).Foreground(lipgloss.Color("#ED8796"))
	HelpStyle           = lipgloss.NewStyle().MarginLeft(2)
)
