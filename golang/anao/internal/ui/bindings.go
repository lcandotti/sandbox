package ui

import "github.com/charmbracelet/bubbles/key"

// KeyMap defines a set of key bindings. To work for help it must satisfy
// key.Map interface. It could also very easily be a map[string]Key.Binding.
type KeyMap struct {
	Up    key.Binding
	Down  key.Binding
	Help  key.Binding
	Enter key.Binding
	Quit  key.Binding
}

// ShortHelp return key bindings to be shown in the mini help view. It's part
// of the key.Map interface.
func (k KeyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Help, k.Quit}
}

// FullHelp returns key bindings for the expanded help view. It's part of the
// key.Map interface
func (k KeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.Up, k.Down},
		{k.Help, k.Enter, k.Quit},
	}
}

var Keys = KeyMap{
	Up: key.NewBinding(
		key.WithKeys("up", "k"),
		key.WithHelp("↑/k", "move up"),
	),
	Down: key.NewBinding(
		key.WithKeys("down", "j"),
		key.WithHelp("↓/j", "move down"),
	),
	Help: key.NewBinding(
		key.WithKeys("?"),
		key.WithHelp("?", "toggle help"),
	),
	Enter: key.NewBinding(
		key.WithKeys("enter"),
		key.WithHelp("enter", "select an option"),
	),
	Quit: key.NewBinding(
		key.WithKeys("q", "esc", "ctrl+c"),
		key.WithHelp("q", "quit the program"),
	),
}
