package main

import (
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/lcandotti/anao/internal/ui"
)

func main() {
	if os.Getenv("TUI_DEBUG") != "" {
		f, err := tea.LogToFile("debug.log", "help")

		if err != nil {
			fmt.Println("Couldn't open a file for logging:", err)
			os.Exit(1)
		}

		defer f.Close()
	}

	p := tea.NewProgram(ui.New(), tea.WithAltScreen())

	if _, err := p.Run(); err != nil {
		fmt.Printf("Error: %v", err)
		os.Exit(1)
	}
}
