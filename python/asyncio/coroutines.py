import asyncio
import random
import string


async def coroutine(identifier):
    """
    Simulates an asynchronous task with a random sleep time.

    Args:
        identifier (str): A unique identifier for the coroutine.

    Returns:
        str: The identifier of the completed coroutine.
    """
    print(">", identifier, "started")
    await asyncio.sleep(random.uniform(0.5, 5))
    print(">", identifier, "finished")
    return identifier


async def manual():
    """
    Demonstrates manually creating and managing asyncio tasks using `asyncio.create_task`.

    This function creates a list of tasks from coroutine objects and waits for all of them to complete.
    """
    tasks = [asyncio.create_task(coroutine(f"Task {letter.upper()}")) for letter in string.ascii_letters[0:5]]

    _, __ = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)


async def gather():
    """
    Demonstrates running coroutines concurrently using `asyncio.gather`.

    This function gathers all coroutine objects into a single awaitable and waits for all of them to complete.
    """
    tasks = [coroutine(f"Task {letter.upper()}") for letter in string.ascii_letters[0:5]]

    _ = await asyncio.gather(*tasks)


async def group():    
    """
    Demonstrates running coroutines concurrently using `asyncio.TaskGroup`.
    
    This function creates a `TaskGroup` and adds coroutine objects to it, allowing them to run concurrently.
    """
    tasks = [coroutine(f"Task {letter.upper()}") for letter in string.ascii_letters[0:5]]

    async with asyncio.TaskGroup() as tg:
        for task in tasks:
            _ = tg.create_task(task)


async def main():
    print("\n===== Manual =====")
    await manual()

    print("\n===== Gather =====")
    await gather()

    print("\n===== Group =====")
    await group()


if __name__ == "__main__":
    asyncio.run(main())
