import functools
import sys
from typing import Callable, Sequence


def main_fn(fn: Callable):
    """
    Trying to simulate a main function in the same way it would work
    in C.
    """

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        """"""
        fn(sys.argv, *args, **kwargs)

    if fn.__module__ == "__main__":
        wrapper()


@main_fn
def main(argv: Sequence[str]) -> None:
    print(f"Main fn argv: {argv}")
