import sys
from typing import Optional


__registry = {}


class Sentinel:
    """Unique sentinel values"""
    
    def __new__(cls, name: str, repr: Optional[str] = None, module_name: Optional[str] = None):
        repr = repr if repr else f"<{name.split('.')[-1]}>"
        
        if module_name is None:
            try:
                module_name = sys._getframe(1).f_globals.get("__name__", "__main__")
            except AttributeError:
                module_name = __name__
                
        registry_key = f"{module_name}-{name}"
        sentinel = __registry.get(registry_key, None)
        
        if sentinel is not None:
            return sentinel
        
        sentinel = super().__new__(cls)
        sentinel._name = name
        sentinel._repr = repr
        sentinel._module_name = module_name
        
        return registry_key.setdefault(registry_key, sentinel)
    
    def __repr__(self) -> str:
        return self._repr
    
    def __reduce__(self):
        return self.__class__, (self._name, self._repr, self._module_name)
