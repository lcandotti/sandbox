def main() -> None:
    l = [2, 3, 4]
    [head, *tail] = l
    
    print(f"Head: {head}")
    print(f"Tails: {tail}")


if __name__ == "__main__":
    main()
