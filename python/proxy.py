from abc import ABC, abstractmethod


class IAccumulator(ABC):
    """
    Dummy interface to test the proxy pattern
    """

    @abstractmethod
    def add(self, __l: int, __r: int) -> int:
        raise NotImplementedError()


class Accumulator(IAccumulator):
    """
    A class that will be consumed by the proxy
    """

    def add(self, __l: int, __r: int) -> int:
        print("Run: accumulator.add(__l, __r)")
        return __l + __r


class Proxy(IAccumulator):
    """"""

    def __init__(self, instance: IAccumulator) -> None:
        self._instance = instance

    def add(self, __l: int, __r: int) -> int:
        print("Run: proxy.accumulator.add(__l, __r)")
        return self._instance.add(__l, __r)


def main() -> None:
    accumulator = Accumulator()
    print(f"From accumulator: {accumulator.add(0, 0)}")

    print()

    proxy_accumulator = Proxy(accumulator)
    print(f"From proxy accumulator: {proxy_accumulator.add(0, 0)}")


if __name__ == "__main__":
    main()
