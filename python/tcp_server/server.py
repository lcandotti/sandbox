import logging
import socket
import sys


HOST = "0.0.0.0"
PORT = 9174


logger = logging.getLogger(__file__)
logging.basicConfig(format="[%(asctime)s] %(levelname)s %(message)s", level=logging.DEBUG)


def main() -> None:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()

        connection, address = s.accept()

        with connection:
            logger.info(f"Connected by {address}")

            while True:
                data = connection.recv(1024)
                if not data:
                    continue

                logger.info(f"Data received: {data}")
                logger.info(f"Data size: {sys.getsizeof(data)}")

                connection.sendall(data)


if __name__ == "__main__":
    main()
