import logging
import socket
import sys
from time import sleep


HOST = "0.0.0.0"
PORT = 9174


logger = logging.getLogger(__file__)
logging.basicConfig(format="[%(asctime)s] %(levelname)s %(message)s", level=logging.DEBUG)


def main() -> None:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))

        while True:
            sleep(1)

            s.sendall(b"Hello, World!")
            data = s.recv(1024)

            logger.info(f"Data received: {data}")
            logger.info(f"Data size: {sys.getsizeof(data)}")


if __name__ == "__main__":
    main()
