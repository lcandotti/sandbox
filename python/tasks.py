import functools
from typing import Callable, Optional


def task(fn: Optional[Callable] = None, thread: bool = True, process: bool = False) -> Callable:
    """
    By default, run the task in a dedicated thread. Be mindfull that
    running the task in a different process would make print statement
    and such unavailable to the terminal where the main thread is running.
    """

    @functools.wraps(fn)
    def wrapper(*args, **kwargs) -> None:
        """"""
        if thread is True:
            from threading import Thread

            thread_ = Thread(target=fn, args=args, kwargs=kwargs)
            thread_.start()
        elif process is True:
            from multiprocessing import Process

            process_ = Process(target=fn, args=args, kwargs=kwargs)
            process_.start()
        else:
            raise ValueError("Unable to start task. No backend provided. Select between thread and multiprocess")

    return wrapper


@task
def benchmark() -> None:
    """"""
    for i in range(100):
        print(f"From: thread 1 - {i}")


def main() -> None:
    benchmark()

    for i in range(100):
        print(f"From: main thread - {i}")


if __name__ == "__main__":
    main()
