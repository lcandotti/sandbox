class A:
    def __init__(self):
        self.ctx = [1, 2, 3]

    def process_a(self, i):
        data = {}

        for j in self.ctx:
            data.update(self.process_b(i, j))

        # Assert that object has correctly been updated
        assert i is None, f"Object {i} has not been updated to 'None' by the process_b method"

    # noinspection PyMethodMayBeStatic
    def process_b(self, i, j):
        if j % 2 == 0:
            # TODO: Update variable 'i' to 'None' from within this function and continue the parent iteration.
            # CONSTRAINTS:
            #     - Update directly from within this block scope
            #     - No return statement available
            return {}

        return {}


if __name__ == "__main__":
    a = A()
    a.process_a(object())
