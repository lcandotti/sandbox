(ns multimethods
  (:gen-class))

(defmulti greeting :type)

(defmethod greeting :human
  [person]
  (str "Hello, " (:name person) "!"))

(defmethod greeting :robot
  [person]
  (str "Kib, kip, kibop " (:name person) "!"))

(def alice {:name "Alice" :type :human})
(def bob {:name "Bob" :type :robot})

(println (greeting alice))
(println (greeting bob))
