(ns apply
  "The `apply` function invoke a function with 0 or more fixed arguments.
  and draws the rest of the needed arguments from a final sequence. The final
  argument must be a sequence."
  (:gen-class))

(defn f
  [x y z]
  (+ x y z))

(apply f '(1 2 3))
(apply f 1 '(2 3))
(apply f 1 2 '(3))

(defn plot
  "Example before using `apply` fn"
  [shape coords] ;; Coords is [x y]
  (plotxy shape (fist coords) (second coords)))

(defn nplot
  "Example after using `apply` fn"
  [shape coords] ;; Coords is [x y]
  (apply plotxy shape coords))
