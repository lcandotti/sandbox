(ns variadics
  (:gen-class))

(defn greet
  [greeting & who]
  ;; Usage: (greet "Hello" "world" "Adam")
  ;; Output: Hello (world Adam)
  (println greeting who))
