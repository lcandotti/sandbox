(ns zip
  (:gen-class))

(defn zip
  [keys values]
  (loop [map {}
         keys (seq keys)
         values (seq values)]
    (if (and keys values)
      (recur (assoc map (first keys) (first values))
             (next keys)
             (next values))
      map)))
