(ns anonymous
  (:gen-class))

;; First example
((fn [message] (println message)) "Hello from anonymous fn")

;; Second example
#(+ 6 %) ;; Eq: (fn [x] (+ 6 x))

;; Third example
#(+ %&) ;; Eq: (fn [& x] (+ x))
