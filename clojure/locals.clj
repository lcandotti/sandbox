(ns locals
  (:gen-class))

;; Define local bound variable that are made available in the defined scope
(let [x 10
      y 20]
  (+ x y))

;; Define global variable
(def pi 3.14)
