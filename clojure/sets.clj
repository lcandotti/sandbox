(ns sets
  (:gen-class))

(defn is-active
  [status]
  (contains? status :active))

(def status #{:active :inactive :pending})

(println "Is active:" (is-active (get status 1)))
