(ns bencoder
  (:gen-class))

;; multimethod to handle bencode structure
(defmulti bencoder class)

;; implementation of bencode for each type
(defmethod bencoder java.lang.Long
  [n]
  (str "i" n "e"))

(defmethod bencoder java.lang.String
  [s]
  (str (count s) ":" s))

(defmethod bencoder clojure.lang.PersistentVector
  [v]
  (str "l"
    (apply str (map bencoder v)) "e"))

(defmethod bencoder clojure.lang.PersistentArrayMap
  [m]
  (str "d"
    (apply str (map (fn [[k v]]
                      (str (bencoder k)
                        (bencoder v))) m))
    "e"))

;; Usage
(println (bencoder {"user" "John"
                    "nbr_of_downloads" 632
                    "nbr_of_uploads" 28
                    "preferred_categories" ["programming" "games"]}))
