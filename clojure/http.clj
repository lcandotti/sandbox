(ns http
  (:gen-class))

(import '[java.net URL])

(defn http-get
  [url]
  (let [u (URL. url)]
    (slurp (.openStream u))))

(assert (.contains (http-get "https://www.w3.org") "html") "Expected valid HTML document")
