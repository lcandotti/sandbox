(ns atom
  (:gen-class))

(def ^:const SUBSCRIPTION_COST 30)
(def user (atom {:name "John Doe"
                 :balance 0
                 :state :no-subscription}))

(defn pay
  [user amount]
  (swap! user update-in [:balance] + amount)
  (when (and (>= (:balance @user) SUBSCRIPTION_COST)
             (= :no-subscription (:state @user)))
    (swap! user assoc :state :subscription)
    (swap! user update-in [:balance] - SUBSCRIPTION_COST)))
