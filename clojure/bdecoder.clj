(ns bdecoder
  (:gen-class)
  (:require [clojure.string :as str]))

(defn strip
  [s]
  (subs s 1 (dec (count s))))

(defmulti bdecoder :type)

(defn decode
  [data]
  (case (first data)
    \i (bdecoder {:type :int :value data})
    \l (bdecoder {:type :vector :value data})
    \d (bdecoder {:type :arraymap :value data})
    (bdecoder {:type :str :value data})))

(defmethod bdecoder :int
  [s]
  (let [rest-chars (take-while #(not= % \e) (rest (:value s)))
        res (str/join "" rest-chars)]
    res))

(defmethod bdecoder :str
  [s]
  (let [ss (str/split (:value s) (re-pattern ":"))
        length (Integer/parseInt (first ss))
        value (last (subs ss 0 length))]
    value))

(defmethod bdecoder :vector
  [s]
  (let [res (map decode [(strip (:value s))])]
    res))

(defmethod bdecoder :arraymap
  [_s])

(println (decode "4:eggs"))
;; (println (decode "i64e"))
;; (println (decode "li128ee"))
;; (println (decode "li2048ei2048ee"))
;; (println (decode "d3:cow3:moo4:spami256eli32ei64e4:useree"))
