(ns pipeline
  (:gen-class))

(defn even []
  (->> (range 0 11)
       (filter even?)
       (map (fn [x] (* x x))))

  (even))
