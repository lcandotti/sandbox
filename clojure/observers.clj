(ns observers
  (:gen-class))

(def observers (atom #{}))

(defn add
  [observer]
  (swap! observers conj observer))

(defn notify
  [user]
  (map #(apply % user) @observers))

(defn add-money
  [user amount]
  (swap! user
         (fn [m]
           (update-in m [:balance] + amount)))
  (if (> amount 100)
    (notify user)
    (println "No notification")))
