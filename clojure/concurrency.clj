(ns concurrency
  (:gen-class))

(def numbers [1 2 3 4 5])

(defn square
  [number]
  (* number number))

(defn square-numbers
  [numbers]
  (pmap square numbers))

(println (square-numbers numbers))
