(ns immutable
  (:gen-class))

(defn copy []
  (let [mvector [1 2 3 4 5]
        mmap {:__name__ "Fred"}
        mlist (list 1 2 3 4 5)]
    (list
     (conj mvector 6)
     (assoc mmap :__age__ 30)
     (conj mlist 6))))
