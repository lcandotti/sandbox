#!/usr/bin sh

set -e

usage () {
cat << EOF

Usage: $0 [-h|--help]

Options:
  -h, --help : Display this help message
EOF  
}

install_packages () {
  sudo pacman -Syu
  sudo pacman -S helix
}

main () {
  if [ $# -ne 1 ]; then
    usage
  else
    if [[ "$1" == "-h" || "$1" == "--help" ]]; then
      usage
      exit 0
    fi

    setup
  fi
}

main "$@"
