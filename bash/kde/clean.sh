#!/usr/bin/env bash

set -e

declare -A colors=([green]="\e[32m" [yellow]="\e[33m" [red]="\e[31m" [end]="\e[0m")

function print_header() {
cat << EOF
 _______  ___      _______  _______  __    _         __   __  _______ 
|       ||   |    |       ||   _   ||  |  | |       |  | |  ||       |
|       ||   |    |    ___||  |_|  ||   |_| | ____  |  | |  ||    _  |
|       ||   |    |   |___ |       ||       ||____| |  |_|  ||   |_| |
|      _||   |___ |    ___||       ||  _    |       |       ||    ___|
|     |_ |       ||   |___ |   _   || | |   |       |       ||   |    
|_______||_______||_______||__| |__||_|  |__|       |_______||___|

EOF
}

main () {
    print_header
    
    echo -en "${colors[yellow]}!${colors[end]} - Would you like to clean kde applications bloat (y/N): "
    read -r answer
    
    if [ "$answer" == "y" ]; then
        sudo pacman -Rns arianna angelfish artikulate audex audiocd-kio audiotube blinken bomber bovo cantor falkon ghostwriter granatier itinerary k3b kajongg kalm kanagram kapman kasts katomic kalzium kblackbox kblocks kbounce kbreakout kbruch kdiamond kfourinline kgeography kgoldrunner khangman kig kigo killbots kiriki kiten kjumpingcube kleopatra klettres klickety klines kmahjongg kmplot knavalbattle knights kolf kollision kpat kreversi kshisen ksirk ksnakeduel kspaceduel ksquares ksudoku kturtle ktuberling kubrick kwordquiz lskat marble palapeli parley picmi  
    fi
    
    echo ""
}

main
