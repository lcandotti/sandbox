@ECHO OFF

:: Global settings

SETLOCAL

SET gpu=""

:: Entrypoint

CALL:MAIN
EXIT /B %ERRORLEVEL%

:: Functions

:PRINT_HEADER
ECHO ""
ECHO " _______  _______  __   __  _______  __   __    __   __  ___  "
ECHO "|       ||       ||  |_|  ||       ||  | |  |  |  | |  ||   | "
ECHO "|       ||   _   ||       ||    ___||  |_|  |  |  | |  ||   | "
ECHO "|       ||  | |  ||       ||   |___ |       |  |  |_|  ||   | "
ECHO "|      _||  |_|  ||       ||    ___||_     _|  |       ||   | "
ECHO "|     |_ |       || ||_|| ||   |      |   |    |       ||   | "
ECHO "|_______||_______||_|   |_||___|      |___|    |_______||___| "
ECHO ""
EXIT /B 0

:_DETECT_GPU
:: Vendor - dxdiag
START "" /w dxdiag /t "%cd%\display.txt"
FIND "Card name" display.txt | FIND /V "---------- DISPLAY.TXT" > output.txt
FOR /f "tokens=2,3 delims=:" %%a IN (output.txt) DO gpu=%%a
DEL display.txt /f
DEL output.txt /f
EXIT /B 0

:_VALIDATE_AMD_DEPENDENCIES
ECHO "_VALIDATE_AMD_DEPENDENCIES Not implemented"
EXIT /B 0

:_VALIDATE_NVIDIA_DEPENDENCIES
ECHO "_VALIDATE_NVIDIA_DEPENDENCIES Not implemented"
EXIT /B 0

:_VALIDATE_DEPENDENCIES
CALL:_DETECT_GPU

IF "%gpu%" EQU "amd" (
    CALL:_VALIDATE_AMD_DEPENDENCIES
    EXIT /B 0
)

IF "%gpu%" EQU "nvidia" (
    CALL:_VALIDATED_NVIDIA_DEPENDENCIES
    EXIT /B 0
)

CALL:PANIC "Unknown GPU %gpu%. Aborting process"
EXIT /B 0

:RUN_VALIDATION
CALL:_VALIDATE_DEPENDENCIES

IF ERRORLEVEL 1 GOTO :EOF
GOTO END
EXIT /B 0

:MAIN
IF "%~1" EQU "/?" (
    CALL:USAGE
    EXIT /B 0
)

CALL:PRINT_HEADER
CALL:RUN_VALIDATION

:PANIC
ECHO "Panic: %~1"
EXIT /B 1

:END
EXIT /B 0
