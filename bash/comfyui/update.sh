#!/usr/bin/env bash

set -e

# Global variables
base_dir=""

declare -A colors=([green]="\e[32m" [yellow]="\e[33m" [red]="\e[31m" [end]="\e[0m")

# Functions
usage () {
cat << EOF

Usage: $0 [-h|help]

Options:
  -h, --help : Display this help message
EOF
}

print_header () {
cat << EOF
 _______  _______  __   __  _______  __   __    __   __  ___
|       ||       ||  |_|  ||       ||  | |  |  |  | |  ||   | 
|       ||   _   ||       ||    ___||  |_|  |  |  | |  ||   | 
|       ||  | |  ||       ||   |___ |       |  |  |_|  ||   | 
|      _||  |_|  ||       ||    ___||_     _|  |       ||   | 
|     |_ |       || ||_|| ||   |      |   |    |       ||   | 
|_______||_______||_|   |_||___|      |___|    |_______||___|

EOF
}

update () {
    print_header

    echo -en "${colors[yellow]}!${colors[end]} - Would you like to update ComfyUI (y/N): "
    read -r answer
    echo ""

    if [ "$answer" == "y" ]; then
        cd "$base_dir"
    
        git pull
    
        echo ""
        echo -e "${colors[green]}✓${colors[end]} - ComfyUI has been updated"
    fi
}

main () {
  if [ $# -ne 1 ]; then
      usage
  else
      if [[ "$1" == "-h" || "$1" == "--help" ]]; then
        usage
        exit 0
      fi

      update
  fi
}

main "$@"
