#!/usr/bin/env bash

set -e

# Define all the global variables.
base_dir=""

gpu=""
platform=""
os=""

amd_dependencies="rocm-hip-sdk rocm-opencl-sdk"
nvidia_dependencies="nvidia nvidia-utils"

declare -A colors=([green]="\e[32m" [yellow]="\e[33m" [red]="\e[31m" [end]="\e[0m")

# This function displays the usage information for the script.
usage () {
cat << EOF

Usage: $0 <install_path> [-h|help]

Arguments:
  install_path: The path where to install ComfyUI. (No need for trailing slashes)

Options:
  -h, --help : Display this help message
EOF
}

print_header () {
cat << EOF
 _______  _______  __   __  _______  __   __    __   __  ___
|       ||       ||  |_|  ||       ||  | |  |  |  | |  ||   |
|       ||   _   ||       ||    ___||  |_|  |  |  | |  ||   |
|       ||  | |  ||       ||   |___ |       |  |  |_|  ||   |
|      _||  |_|  ||       ||    ___||_     _|  |       ||   |
|     |_ |       || ||_|| ||   |      |   |    |       ||   |
|_______||_______||_|   |_||___|      |___|    |_______||___|

EOF
}

print_installation_header () {
cat << EOF
 ___   __    _  _______  _______  _______  ___      ___      _______  _______  ___   _______  __    _
|   | |  |  | ||       ||       ||   _   ||   |    |   |    |   _   ||       ||   | |       ||  |  | |
|   | |   |_| ||  _____||_     _||  |_|  ||   |    |   |    |  |_|  ||_     _||   | |   _   ||   |_| |
|   | |       || |_____   |   |  |       ||   |    |   |    |       |  |   |  |   | |  | |  ||       |
|   | |  _    ||_____  |  |   |  |       ||   |___ |   |___ |       |  |   |  |   | |  |_|  ||  _    |
|   | | | |   | _____| |  |   |  |   _   ||       ||       ||   _   |  |   |  |   | |       || | |   |
|___| |_|  |__||_______|  |___|  |__| |__||_______||_______||__| |__|  |___|  |___| |_______||_|  |__|

EOF
}

print_post_installation_header () {
cat << EOF
 _______  _______  _______  _______         ___   __    _  _______  _______  _______  ___      ___      _______  _______  ___   _______  __    _
|       ||       ||       ||       |       |   | |  |  | ||       ||       ||   _   ||   |    |   |    |   _   ||       ||   | |       ||  |  | |
|    _  ||   _   ||  _____||_     _| ____  |   | |   |_| ||  _____||_     _||  |_|  ||   |    |   |    |  |_|  ||_     _||   | |   _   ||   |_| |
|   |_| ||  | |  || |_____   |   |  |____| |   | |       || |_____   |   |  |       ||   |    |   |    |       |  |   |  |   | |  | |  ||       |
|    ___||  |_|  ||_____  |  |   |         |   | |  _    ||_____  |  |   |  |       ||   |___ |   |___ |       |  |   |  |   | |  |_|  ||  _    |
|   |    |       | _____| |  |   |         |   | | | |   | _____| |  |   |  |   _   ||       ||       ||   _   |  |   |  |   | |       || | |   |
|___|    |_______||_______|  |___|         |___| |_|  |__||_______|  |___|  |__| |__||_______||_______||__| |__|  |___|  |___| |_______||_|  |__|

EOF
}

# This function installs the necessary dependencies for AMD GPUs.
install_amd_dependencies () {
  if [ "$os" == "Arch" ]; then
     # shellcheck disable=SC2086 # Using double quote is messing up the command
    sudo pacman -Sy $amd_dependencies
  else
    echo -e "${colors[red]}✗${colors[end]} - Unsupported OS $os. Please install ROCm ($amd_dependencies) manually."
    exit 1
  fi
}

install_nvidia_dependencies () {
  if [ "$os" == "Arch" ]; then
    # shellcheck disable=SC2086 # Using double quote is messing up the command
    sudo pacman -Sy $nvidia_dependencies
  else
    echo -e "${colors[red]}✗${colors[end]} - Unsupported OS $os. Please install Nvidia drivers ($nvidia_dependencies) manually."
    exit 1
  fi
}

# This function checks if ROCm is installed on the system.
# If ROCm is not installed, it prompts the user to install it.
# If the user agrees, it calls the `install_amd_dependencies` function.
# If the user disagrees, it prints an error message and exits the script.
validate_amd_dependencies () {
  if [[ -x "$(command -v rocminfo)" && -x "$(command -v rocm-smi)" ]]; then
    echo -e "${colors[green]}✓${colors[end]} - Detected ROCm installation."
  else
    echo -e "${colors[red]}✗${colors[end]} - ROCm is not installed."

    echo ""
    echo -en "${colors[yellow]}!${colors[end]} - Do you want to install ROCm? (y/N): "
    read -r answer
    echo ""

    if [ "$answer" == "y" ]; then
      install_amd_dependencies
    else
      echo -e "${colors[red]}✗${colors[end]} - ROCm is required to run this script. Please install ROCm and try again."
      exit 1
    fi
  fi
}

# This function checks if Nvidia drivers are installed on the system.
# If Nvidia drivers are not installed, it prompts the user to install them.
# If the user agrees, it calls the `install_nvidia_dependencies` function.
# If the user disagrees, it prints an error message and exits the script.
validate_nvidia_dependencies () {
  if [[ -x "$(command -v nvidia-smi)" ]]; then
    echo -e "${colors[green]}✓${colors[end]} - Detected Nvidia driver installation."
  else
    echo -e "${colors[red]}✗${colors[end]} - Nvidia drivers are not installed."

    echo ""
    echo -en "${colors[yellow]}!${colors[end]} - Do you want to install Nvidia drivers? (y/N): "
    read -r answer
    echo ""

    if [ "$answer" == "y" ]; then
      install_nvidia_dependencies
    else
      echo -e "${colors[red]}✗${colors[end]} - Nvidia drivers are required to run this script. Please install Nvidia drivers and try again."
      exit 1
    fi
  fi
}

# This function checks if the necessary dependencies for the specified GPU are installed.
# It takes one argument, `gpu`, which should be `1` for AMD or `2` for Nvidia.
# If `gpu` is `1`, it calls the `validate_amd_dependencies` function.
# If `gpu` is `2`, it prints an error message saying that Nvidia GPUs are not supported yet.
# If `gpu` is neither `1` nor `2`, it prints an error message and exits the script.
validate_dependencies () {
  gpu=$1

  if [ "$gpu" == 1 ]; then
    validate_amd_dependencies
  elif [ "$gpu" == 2 ]; then
    validate_nvidia_dependencies
  else
    echo -e "${colors[red]}✗${colors[end]} - Invalid option $gpu. Please choose a valid option."
    exit 1
  fi
}

# This function checks the platform on which the script is running.
# It assigns the output of the `uname` command to the `platform` variable.
validate_platform () {
  platform=$(uname)

  if [ "$platform" != "Linux" ]; then
    echo -e "${colors[red]}✗${colors[end]} - This script is only supported on Linux platform."
    exit 1
  fi

  echo -e "${colors[green]}✓${colors[end]} - Detected platform: $platform"
}

# This function validates the Python installation.
# It checks if Python is installed and exits the script if it's not.
validate_python () {
  local version

  version=$(python3 --version 2>&1 | awk '{print $2}')

  if [ -z "$version" ]; then
    echo "${colors[red]}✗${colors[end]} - Python (>=3.10.X) is must be installed to run this script. Please install python and try again."
    exit 1
  fi

  echo -e "${colors[green]}✓${colors[end]} - Detected python version: $version"
}

# This function validates the operating system.
# It checks if the operating system is supported and exits the script if it's not.
validate_os () {
  os=$(lsb_release -d | awk '{print $2}')

  if [ "$os" != "Arch" ]; then
    echo -e "${colors[red]}✗${colors[end]} - This script is only supported on Arch platform."
    exit 1
  fi

  echo -e "${colors[green]}✓${colors[end]} - Detected OS: $os"
}

# This function runs all the validation functions.
run_validation () {
  validate_platform
  validate_os
  validate_python

  echo ""
  echo -en "${colors[yellow]}!${colors[end]} - Choose your GPU (1=amd, 2=nvidia): "
  read -r answer
  echo ""

  validate_dependencies "$answer"
}

# This function installs ComfyUI.
install_comfyui () {
  git clone https://github.com/comfyanonymous/ComfyUI.git "$base_dir"/comfyui

  python3 -m venv "$base_dir"/comfyui/.venv

  if [ "$gpu" == 1 ]; then
    "$base_dir"/comfyui/.venv/bin/pip install torch torchaudio torchvision --index-url https://download.pytorch.org/whl/rocm6.1
  elif [ "$gpu" == 2 ]; then
    "$base_dir"/comfyui/.venv/bin/pip install torch torchaudio torchvision --index-url https://download.pytorch.org/whl/cu124
  else
    echo -e "${colors[red]}✗${colors[end]} - Unknown GPU $gpu. Aborting process."
    exit 1
  fi

  "$base_dir"/comfyui/.venv/bin/pip install -r "$base_dir"/comfyui/requirements.txt

  cp ./run.sh "$base_dir"/comfyui/
  cp ./update.sh "$base_dir"/comfyui/
}

# This function installs the necessary dependencies.
# It first runs the validation functions, then asks the user to choose their GPU.
# After the user has chosen their GPU, it validates the dependencies for that GPU.
install () {
  print_header
  run_validation
  print_installation_header
  install_comfyui
  print_post_installation_header
  post_install

  echo ""
  echo -e "${colors[green]}✓${colors[end]} - ComfyUI is installed. You can run it by using the provided run.sh script in comfyui directory"
}

# This function should be run as a post install statment. It installs ComfyUI Manager.
install_comfyui_manager () {
  echo -e "${colors[green]}✓${colors[end]} - Installing ComfyUI Manager"
  echo ""

  git clone https://github.com/ltdrdata/ComfyUI-Manager.git $base_dir/comfyui/custom_nodes/ComfyUI-Manager
}

# This function run all the post installation scripts after ComfyUI is setup and ready to use.
post_install () {
  echo -en "${colors[yellow]}!${colors[end]} - Would you like to proceed with the post installation script (y/N): "
  read -r answer
  echo ""

  if [ "$answer" == "y" ]; then
    echo -en "${colors[yellow]}!${colors[end]} - Installing ComfyUI-Manager. Would you like to proceed (y/N): "
    read -r answer
    echo ""

    if [ "$answer" == "y" ]; then
        install_comfyui_manager
    fi
  fi

  echo ""
  echo -e "${colors[green]}✓${colors[end]} - Post installation is done"
}

# This function is the main entry point of the script.
# If the script is called with any arguments, it displays the usage information.
# If the script is called without any arguments, it runs the install function.
main () {
  if [ $# -ne 1 ]; then
      usage
  else
      if [[ "$1" == "-h" || "$1" == "--help" ]]; then
        usage
        exit 0
      fi

      base_dir=$1

      install
  fi
}

main "$@"
