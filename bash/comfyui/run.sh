#!/usr/bin/env bash

set -e

main () {
    .venv/bin/python main.py "$@"
}

main "$@"
