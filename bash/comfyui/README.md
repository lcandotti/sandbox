# ComfyUI Automatic Installer

Discover the joy of a ComfyUI install w/o any hassle. Tailored to your system, this script will surely make all those process stress free.

## Prerequisites

- git

### For linux only

- lsb_release

## Available platform

- Windows (WIP)
- Linux
  - Arch


## Available GPU

- AMD
- Nvidia (Linux: OK, Windows: NOK, MacOS: NOK)
