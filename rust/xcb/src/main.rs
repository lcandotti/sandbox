use std::fs;
use std::io::Cursor;
use std::path::Path;

use image::io::Reader;
use xcb::x;
use xcb::Xid;

/// Create a xcb::x::Window object
///
/// # Arguments
///
/// * `conn` - Connection to the X server
/// * `screen` - The screen where the window is living
/// * `width` - Width the window should have
/// * `height` - Height the window should have
///
/// # Examples
/// ```
/// /// Establishing a connection to X server
/// let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
///
/// /// Fetch the `x::Setup` and get the main `x::Screen` object.
/// let setup = conn.get_setup();
/// let screen = setup.roots().nth(screen_num as usize).unwrap();
///
/// /// Get a new window
/// let window = create_window(&conn, screen, 150, 150);
/// ```
fn create_window(conn: &xcb::Connection, screen: &x::Screen, width: u16, height: u16) -> x::Window {
    // Generate an `Xid` for the client window.
    // The type inference is maybe needed here.
    let window = conn.generate_id();

    // We can now create a window. For this we pass a `Request`
    // object to the `send_request_checked` method. The method
    // returns a cookie that will be used to check for success.
    let cookie = conn.send_request_checked(&x::CreateWindow {
        depth: x::COPY_FROM_PARENT as u8,
        wid: window,
        parent: screen.root(),
        x: 0,
        y: 0,
        width,
        height,
        border_width: 0,
        class: x::WindowClass::InputOutput,
        visual: screen.root_visual(),
        // This list must be in same order than `Cw` enum order
        value_list: &[
            x::Cw::BackPixel(screen.white_pixel()),
            x::Cw::EventMask(x::EventMask::EXPOSURE | x::EventMask::KEY_PRESS),
        ],
    });

    conn.check_request(cookie)
        .expect("Unable to create a new window");

    window
}

/// Create a xcb::x::Gc and the corresponding xcb_image_t to put it on the provided
/// connection manager and screen
///
/// # Arguments
///
/// * `conn` - The connection handler to the X server
/// * `window` - The window object that image is gonna be mapped on
/// * `img_path` - The absolute path to the image to load
fn put_image(conn: &xcb::Connection, window: x::Window, img_path: &Path) {
    // Load image file into a buffer
    let file = fs::read(img_path).unwrap();
    let img = Reader::new(Cursor::new(file))
        .with_guessed_format()
        .unwrap()
        .decode()
        .unwrap();

    // Reserve a memory address for our graphical context in the X system
    let gc: x::Gcontext = conn.generate_id();

    // Send request to create a new graphical context
    let cookie = conn.send_request_checked(&x::CreateGc {
        cid: gc,
        drawable: x::Drawable::Window(window),
        value_list: &[],
    });

    conn.check_request(cookie)
        .expect("Unable to create Gcontext");

    // Convert the image into a raw buffer
    let _ = img.into_rgba8().into_raw();

    // Convert the raw buffer into an xcb image
}

/// Run an event loop on the main thread for a window
///
/// # Arguments
///
/// * `conn` - Connection to the X server
/// * `screen` - Screen where the window will display
/// * `window` - Window to display
///
/// # Examples
/// ```
/// /// Establishing a connection to the X server
/// let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
///
/// /// Fetch the `x::Setup` and get the main `x::Screen` object.
/// let setup = conn.get_setup();
/// let screen = setup.roots().nth(screen_num as usize).unwrap();
///
/// /// Get a new window
/// let window = create_window(&conn, screen, 150, 150);
///
/// /// Run the main event loop
/// wloop(&conn, screen, window);
/// ```
fn wloop(conn: &xcb::Connection, screen: &x::Screen, window: x::Window) {
    // We now show ("map" in X terminology) the window.
    // This time we do not check for success, so we discard the cookie.
    conn.send_request(&x::MapWindow { window });

    // We need a few atoms for our application.
    // We send a few requests in a row and wait for the replies after.
    let (wm_protocols, wm_del_window, wm_state, wm_state_maxv, wm_state_maxh) = {
        let cookies = (
            conn.send_request(&x::InternAtom {
                only_if_exists: true,
                name: b"WM_PROTOCOLS",
            }),
            conn.send_request(&x::InternAtom {
                only_if_exists: true,
                name: b"WM_DELETE_WINDOW",
            }),
            conn.send_request(&x::InternAtom {
                only_if_exists: true,
                name: b"_NET_WM_STATE",
            }),
            conn.send_request(&x::InternAtom {
                only_if_exists: true,
                name: b"_NET_WM_STATE_MAXIMIZED_VERT",
            }),
            conn.send_request(&x::InternAtom {
                only_if_exists: true,
                name: b"_NET_WM_STATE_MAXIMIZED_HORZ",
            }),
        );
        (
            conn.wait_for_reply(cookies.0).unwrap().atom(),
            conn.wait_for_reply(cookies.1).unwrap().atom(),
            conn.wait_for_reply(cookies.2).unwrap().atom(),
            conn.wait_for_reply(cookies.3).unwrap().atom(),
            conn.wait_for_reply(cookies.4).unwrap().atom(),
        )
    };

    // We now activate the window close event by sending the following request.
    // If we don't do this we can still close the window by clicking on the "x" button,
    // but the event loop is notified through a connection shutdown error.
    conn.check_request(conn.send_request_checked(&x::ChangeProperty {
        mode: x::PropMode::Replace,
        window,
        property: wm_protocols,
        r#type: x::ATOM_ATOM,
        data: &[wm_del_window],
    }))
    .expect("Unable to change property");

    // Previous request was checked, so a flush is not necessary in this case.
    // Otherwise, here is how to perform a connection flush.
    // conn.flush()
    //    .expect("Unable to flush connection to X server");

    let mut maximized = false;

    // We enter the main event loop
    loop {
        match conn.wait_for_event().unwrap() {
            xcb::Event::X(x::Event::KeyPress(ev)) => {
                if ev.detail() == 0x3a {
                    // The M key was pressed
                    // (M only on qwerty keyboards. Keymap support is done
                    // with the `xkb` extension and the `xkbcommon-rs` crate)

                    // We toggle maximized state, for this we send a message
                    // by building a `x::ClientMessageEvent` with the proper
                    // atoms and send it to the server.

                    let data = x::ClientMessageData::Data32([
                        if maximized { 0 } else { 1 },
                        wm_state_maxv.resource_id(),
                        wm_state_maxh.resource_id(),
                        0,
                        0,
                    ]);

                    let event = x::ClientMessageEvent::new(window, wm_state, data);
                    let cookie = conn.send_request_checked(&x::SendEvent {
                        propagate: false,
                        destination: x::SendEventDest::Window(screen.root()),
                        event_mask: x::EventMask::STRUCTURE_NOTIFY,
                        event: &event,
                    });

                    conn.check_request(cookie).expect("Unable to send event");

                    // Same as before, if we don't check for error, we have to flush
                    // the connection.
                    conn.flush()
                        .expect("Unable to flush connection to X server");

                    maximized = !maximized;
                } else if ev.detail() == 0x18 {
                    // Q (on qwerty)

                    // We exit the event loop (and the program)
                    break;
                }
            }
            xcb::Event::X(x::Event::ClientMessage(ev)) => {
                // We have received a message from the server
                if let x::ClientMessageData::Data32([atom, ..]) = ev.data() {
                    if atom == wm_del_window.resource_id() {
                        // The received atom is "WM_DELETE_WINDOW".
                        // We can check here if the user needs to save before
                        // exit, or in our case, exit right away.
                        break;
                    }
                }
            }
            _ => {}
        }
    }
}

fn main() {
    // Establishing a connection to the X server
    let (conn, screen_num) = xcb::Connection::connect(None).unwrap();

    // Fetch the `x::Setup` and get the main `x::Screen` object.
    let setup = conn.get_setup();
    let screen = setup.roots().nth(screen_num as usize).unwrap();

    // Get a new window
    let window = create_window(&conn, screen, 150, 150);

    // Set an image as the window background
    let path = Path::new("./assets/wp.png");
    put_image(&conn, window, path);

    // Run the main event loop
    wloop(&conn, screen, window);
}
