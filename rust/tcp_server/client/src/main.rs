use std::str;
use std::thread;
use std::time::{Duration, SystemTime};
use std::net::TcpStream;
use std::io::Write;

use time::{format_description, OffsetDateTime};

const LOG_FORMAT_TIME: &str = "[year]-[month]-[day] [hour]:[minute]:[second]";

fn main() {
    match TcpStream::connect("0.0.0.0:9600") {
        Ok(mut stream) => {
            let message = b"Hello, World";

            loop {
                let date_time: OffsetDateTime = SystemTime::now().into();
                let date_format = format_description::parse(LOG_FORMAT_TIME).unwrap();

                println!(
                    "[{}] Send ({}) {}",
                    date_time.format(&date_format).unwrap(),
                    message.len(),
                    str::from_utf8(message).unwrap()
                );

                stream.write_all(message).unwrap();

                let seconds = Duration::new(1, 0);  // => 1 second
                thread::sleep(seconds);
            }
        },
        Err(_) => { println!("Failed to connect") }
    }
}
