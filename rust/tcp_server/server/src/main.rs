use std::str;
use std::thread;
use std::time::SystemTime;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream, Shutdown};

use time::{format_description, OffsetDateTime};

const LOG_FORMAT_TIME: &str = "[year]-[month]-[day] [hour]:[minute]:[second]";

fn handle_stream(mut stream: TcpStream) {
    let mut buffer = [0;1024];

    while match stream.read(&mut buffer) {
        Ok(size) => {
            let date_time: OffsetDateTime = SystemTime::now().into();
            let date_format = format_description::parse(LOG_FORMAT_TIME).unwrap();

            println!(
                "[{}] Recv ({}) {}",
                date_time.format(&date_format).unwrap(),
                size,
                str::from_utf8(&buffer).unwrap()
            );

            stream.write_all(&buffer[0..size]).unwrap();

            println!(
                "[{}] Send ({}) {}",
                date_time.format(&date_format).unwrap(),
                buffer.len(),
                str::from_utf8(&buffer).unwrap()
            );

            true
        },
        Err(_) => {
            println!("An error occurred, terminating connection with {}", stream.peer_addr().unwrap());
            stream.shutdown(Shutdown::Both).unwrap();

            false
        }
    } {}
}

fn main() {
    println!("Starting server at 0.0.0.0:9600");
    println!("Quit the server with CONTROL-C");

    let listener = TcpListener::bind("0.0.0.0:9600").unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr().unwrap());
                thread::spawn(move || { handle_stream(stream) });
            },
            Err(e) => { println!("Error: {}", e) }
        }
    }

    drop(listener);
}
