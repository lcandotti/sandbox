-module(main).
-compile(no_auto_import).

-export([main/0]).

-spec main() -> nil.
main() -> 
  io:fwrite("Hello, World!\n").
